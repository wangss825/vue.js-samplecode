/**
 * Function called to populate stories database
 * @param db - DB instance
 */
function populate(db)
{

    db.bulkDocs([
        {
            "_id": "01-EMPTY",

            "Steps": []
        },
        {
            "_id": "02-DUPLICATE",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                },
                {
                    "stepID": 1,
                    "stepTitle": "step 2",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }
            ]
        },
        {
            "_id": "03-EMPTYID",

            "Steps": [
                {
                    "stepID": '',
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "04-NANID",

            "Steps": [
                {
                    "stepID": '1#A',
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "05-OVERMAXID",

            "Steps": [
                {
                    "stepID": 1001,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "06-MAXID",

            "Steps": [
                {
                    "stepID": 1000,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }
            ]
        },
        {
            "_id": "07-WIDTHOVERMAX",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 401,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "08-WIDTHATMAX",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 400,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "09-WIDTHATMIN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "10-WDTHBELOWMIN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 149,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "11-WIDTHNAN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 'A',
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "12-HEIGHTATMAX",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 150,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "13-HGHTOVERMAX",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 151,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "14-HEIGHTATMIN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 40,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "15-HGHTBELOWMIN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 39,
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "16-HEIGHTNAN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 'A',
                    "stepXPosition": 30,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "17-XPOSNAN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 'hello',
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "18-XPOSOVERMAX",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 1921,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "19-XPOSATMAX",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 1920,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "20-XPOSATMIN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 0,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "21-XPOSBELOWMIN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": -1,
                    "stepYPosition": 40,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "22-YPOSNAN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 'hello',
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "23-YPOSATMAX",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 969,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "24-YPOSOVERMAX",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 970,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "25-YPOSATMIN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 100,
                    "stepXPosition": 30,
                    "stepYPosition": 0,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },
        {
            "_id": "26-YPOSBELOWMIN",

            "Steps": [
                {
                    "stepID": 1,
                    "stepTitle": "step 1",
                    "stepDesc": "This is the first step",
                    "stepURL": "www.google.com",
                    "stepColor": "#000000",
                    "stepWidth": 150,
                    "stepHeight": 60,
                    "stepXPosition": 30,
                    "stepYPosition": -1,
                    "stepComment": "Hello everybody",
                    "checklist": []
                }

            ]
        },


    ], function (err, response) {
        if (err) {
            return console.log(err);
        }
    });

}

/**
 * Will clear the database
 * @param db - DB instance
 */
function unpopulate(db)
{

    var docs = db.allDocs().then(function(response){
        return response.rows;
    }).then(function(docs){
        for(i=0; i< docs.length; i++)
        {
            if(docs[i].id !== '_design/auth')
            {
                db.get(docs[i].id).then(function(doc){
                    return db.remove(doc);
                })
            }
        }
    })
}