 //setting for selenium webdriver
var assert = require('assert');
var webdriver = require('selenium-webdriver'),
    chrome = require('selenium-webdriver/chrome'),
    options = new chrome.Options();
options.addArguments([
   '--headless',
    '--disable-extensions',
    '--auto-open-devtools-for-tabs',
    '--remote-debugging-address=0.0.0.0'
]).windowSize({height: 1024, width: 1200});

var service = new chrome.ServiceBuilder("C:/Users/IEUser/AppData/Local/Microsoft/WindowsApps/chromedriver.exe").build();
var driver = chrome.Driver.createSession(options, service);

 //loading fixtures js file dynamically and ready to use attributes and methods inside this js file
var fs = require('fs');
var vm = require('vm');
var path ='./test/fixtures/story4r_fixtures.js'; //path for story4r_fixtures.js
var code = fs.readFileSync(path);
vm.runInThisContext(code);
 //before do test, create database and write in four documents in couchdb
PouchDB = require('pouchdb');
var db = new PouchDB("http://admin:TurnKey15@192.168.5.129/workflow");
//use aync/await based tesing with Mocha to run front-end test
describe("story4r remove workflow from database test suite", function(){
    before(async function(){

        //unpopulate(DB);
        populate(db); //put 4 documents to database
        //set driver sleep for a short time to wait database setup before index page open
        driver.sleep(1000);

        this.timeout(10000);
        await driver.get("http://localhost:63342/prj1-ird/index.html");
        driver.sleep(200);
        var editModeButton = await driver.findElement(webdriver.By.css("#btnEditMode"));
        editModeButton.click();

        //send correct credentials
        var inputUserName = await driver.findElement(webdriver.By.css("#inputUsername")).sendKeys("admin");
        var inputPassword = await driver.findElement(webdriver.By.css("#inputPassword")).sendKeys("TurnKey15");
        await driver.findElement(webdriver.By.css("#btnCredentialsOK")).click();

    });

    after(async function(){
        driver.quit();
        //clear database after test
        unpopulate(db);
    });
//User sees a list of workflows as test1-default
   it("User sees a list of workflows as 'aaaaaaaaaaaaaaaaa', 'test1-default', 'test2' ", async function() {


        list = await driver.findElements(webdriver.By.css("#displayworkflow option"));
        driver.sleep(500);
        await driver.findElements(webdriver.By.css('option')).then(async function (list) {
            await driver.sleep(500);
           //list[0] hold a plain text '...', that's why list number start with 1
            assert.strictEqual(await list[1].getText(), 'aaaaaaaaaaaaaaaa');
            assert.strictEqual(await list[2].getText(), 'test1-default');
            assert.strictEqual(await list[3].getText(), 'test2');


        });
       driver.sleep();
    });


   // Test 'Remove' button shows up after user click test2
    it("Test 'Remove' button shows up after user click test2", async function(){

        await driver.findElements(webdriver.By.css('option')).then(async function(list) {
            list[3].click();
            existed = await driver.findElement(webdriver.By.id("rm")).then(function() {
                return true;//it was found
            }, function(err) {
                if (err instanceof webdriver.error.NoSuchElementError) {
                    return false;//element did not exist
                }
            });
            assert.strictEqual(existed,true);
        });

        driver.sleep();
    });
 //Test modal window shows up which has text and two buttons after user click test2
    it("Test modal window shows up which has text and two buttons after user click test2", async function(){
        await driver.findElements(webdriver.By.css('option')).then(async function(list) {
            list[3].click();
        });

        var rm = await driver.findElement(webdriver.By.id("rm"));


        rm.click();
        existed = await driver.findElement(webdriver.By.className("modal-dialog")).then( function() {
            return true;//it was found
        }, function(err) {
            if (err instanceof webdriver.error.NoSuchElementError) {
                return false;//element did not exist
            }
        });
        assert.strictEqual(existed,true);
        modalText = await driver.findElement(webdriver.By.className('modal-body')).getText();
        assert.strictEqual(modalText,"Do you want to delete this workflow test2?");
        modalBtn = await driver.findElements(webdriver.By.className('btn btn-danger'));
        assert.strictEqual(modalBtn.length, 2);
        assert.strictEqual(await modalBtn[0].getText(), 'Yes');
        assert.strictEqual(await modalBtn[1].getText(), 'No');
        await modalBtn[1].click();
        driver.sleep();
    });


//Test  user click on 'No' button  and 'test2' still in the list
    it("Test  user click on 'No' button  and 'test2' still in the list", async function(){
        await driver.findElements(webdriver.By.css('option')).then(async function(list) {
                      list[3].click();
                  })

        var rm = await driver.findElement(webdriver.By.id("rm"));
        rm.click();
        modalBtn = await driver.findElements(webdriver.By.className('btn btn-danger'));
        await modalBtn[1].click();
        list =  await driver.findElements(webdriver.By.css("option"));
        //assert.strictEqual(list.length, 4);
        assert.strictEqual(await list[1].getText(), 'aaaaaaaaaaaaaaaa');
        assert.strictEqual(await list[2].getText(), 'test1-default');
        assert.strictEqual(await list[3].getText(), 'test2');

        driver.sleep();
    });
  //  Test user click on 'Yes' button  and 'test3-deleteme' is not in the list
    it("Test user click on 'Yes' button  and 'test3-deleteme' is not in the list", async function(){
        //list =  await driver.findElements(webdriver.By.css("option"));
        await driver.findElements(webdriver.By.css('option')).then(async function(list) {
            list[4].click();
        })
        //assert.strictEqual(await list[4].getText(), 'test3-deleteme');
       // list[4].click();
        var rm = await driver.findElement(webdriver.By.id("rm")).click();
         modalBtn = await driver.findElements(webdriver.By.className('btn btn-danger'));

         await modalBtn[0].click();
         driver.sleep(500);
         list =  await driver.findElements(webdriver.By.css("#selected > option"));
        assert.strictEqual(list.length, 4);
        assert.strictEqual(await list[1].getText(), 'aaaaaaaaaaaaaaaa');
        assert.strictEqual(await list[2].getText(), 'test1-default');
        assert.strictEqual(await list[3].getText(), 'test2');
        driver.sleep();
        });
//Test  no 'remove' button appear when user click 'test1-default'
    it("Test  no 'remove' button appear when user click 'test1-default'", async function() {

        list =  await driver.findElements(webdriver.By.css("option"));
        await list[2].click();
        driver.sleep(500);
            existed = await driver.findElement(webdriver.By.id("rm")).then(function() {
                return true;//it was found
            }, function(err) {
                if (err instanceof webdriver.error.NoSuchElementError) {
                    return false;//element did not exist
                }

            assert.strictEqual(existed,false);
        });

        driver.sleep();
    });
    //Test error message shows after user click "aaaaaaaaaaaaaaaaaa" workflow
    it("Test error message shows after user click 'aaaaaaaaaaaaaaaaaa' workflow'", async function() {


        await driver.findElements(webdriver.By.css('option')).then(async function(list) {
            list[1].click();
            existed = await driver.findElement(webdriver.By.id("idInvalidError")).then(function() {
                return true;//it was found
            }, function(err) {
                if (err instanceof webdriver.error.NoSuchElementError) {
                    return false;//element did not exist
                }
            });
            assert.strictEqual(existed,true);
        });
        driver.sleep();
    });

});

